"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      mall_ad_imgs: [
        { id: "1", url: "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/027dedf7486746ca8b768923c28b76c5.png?w=2452&h=920" },
        { id: "2", url: "/static/mall_ad_2.png" },
        { id: "3", url: "/static/mall_ad_3.png" },
        { id: "4", url: "/static/mall_ad_4.png" }
      ],
      dotsStyle: {
        backgroundColor: "rgba(0, 0, 0, .3)",
        border: "1px rgba(0, 0, 0, .3) solid",
        color: "#fff",
        selectedBackgroundColor: "rgba(0, 0, 0, .9)",
        selectedBorder: "1px rgba(0, 0, 0, .9) solid"
      },
      current: 0,
      swiperDotIndex: 0
    };
  },
  methods: {
    change(e) {
      this.current = e.detail.current;
    },
    clickItem(e) {
      this.swiperDotIndex = e;
    }
  }
};
if (!Array) {
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  _easycom_uni_swiper_dot2();
}
const _easycom_uni_swiper_dot = () => "../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
if (!Math) {
  _easycom_uni_swiper_dot();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.mall_ad_imgs, (item, k0, i0) => {
      return {
        a: item.url,
        b: item.id
      };
    }),
    b: common_vendor.o((...args) => $options.change && $options.change(...args)),
    c: $data.swiperDotIndex,
    d: common_vendor.p({
      mode: "default",
      info: $data.mall_ad_imgs,
      current: $data.current,
      ["dots-styles"]: $data.dotsStyle
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
